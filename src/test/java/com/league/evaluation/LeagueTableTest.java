package com.league.evaluation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;


/**
 * Created by Vladimir Usychenko on 4/4/16.
 */

@RunWith(MockitoJUnitRunner.class)
public class LeagueTableTest {

    @Before
    public void before() {
        System.out.println("Starting test");
    }

    @After
    public void after() {
        System.out.println("Finishing test");
    }

    @Test
    public void test_LeagueTableEntries_order_with_different_team_names_only() {

        Match match1 = new Match("Team2", "Team1", 1, 0);
        Match match2 = new Match("Team1", "Team2", 1, 0);

        Match[] matches = {
                match1, match2
        };

        List<Match> matchesListSortByTeamName = Arrays.stream(matches).collect(Collectors.toList());

        LeagueTable leagueTable = new LeagueTable(matchesListSortByTeamName);

        List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntries();

        LeagueTableEntry team1 = leagueTableEntries.get(0);
        LeagueTableEntry team2 = leagueTableEntries.get(1);

        // different getTeamName
        assertEquals("Team1", team1.getTeamName());
        assertEquals(1, team1.getGoalsFor());
        assertEquals(1, team1.getGoalsAgainst());
        assertEquals(0, team1.getGoalDifference());
        assertEquals(2, team1.getPlayed());
        assertEquals(3, team1.getPoints());

        // different getTeamName
        assertEquals("Team2", team2.getTeamName());
        assertEquals(1, team2.getGoalsFor());
        assertEquals(1, team2.getGoalsAgainst());
        assertEquals(0, team2.getGoalDifference());
        assertEquals(2, team2.getPlayed());
        assertEquals(3, team2.getPoints());
    }

    @Test
    public void test_LeagueTableEntries_order_with_different_goals_for_only() {

        Match match1 = new Match("Team2", "Team1", 1, 0);
        Match match2 = new Match("Team1", "Team2", 1, 0);
        Match match3 = new Match("Team3", "Team2", 2, 0);
        Match match4 = new Match("Team4", "Team1", 3, 1);

        Match[] matches = {
                match1, match2, match3, match4
        };

        List<Match> matchesListSortByTeamName = Arrays.stream(matches).collect(Collectors.toList());

        LeagueTable leagueTable = new LeagueTable(matchesListSortByTeamName);

        List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntries();

        LeagueTableEntry team1 = leagueTableEntries.get(0);
        LeagueTableEntry team2 = leagueTableEntries.get(1);

        assertEquals("Team4", team1.getTeamName());
        // different getGoalsFor
        assertEquals(3, team1.getGoalsFor());
        assertEquals(1, team1.getGoalsAgainst());
        assertEquals(2, team1.getGoalDifference());
        assertEquals(1, team1.getPlayed());
        assertEquals(3, team1.getPoints());

        assertEquals("Team3", team2.getTeamName());
        // different getGoalsFor
        assertEquals(2, team2.getGoalsFor());
        assertEquals(0, team2.getGoalsAgainst());
        assertEquals(2, team2.getGoalDifference());
        assertEquals(1, team2.getPlayed());
        assertEquals(3, team2.getPoints());

    }

    @Test
    public void test_LeagueTableEntries_order_with_different_goals_difference() {

        Match match1 = new Match("Team2", "Team1", 2, 0);
        Match match2 = new Match("Team1", "Team2", 1, 0);
        Match match3 = new Match("Team3", "Team2", 1, 1);
        Match match4 = new Match("Team2", "Team3", 1, 1);
        Match match5 = new Match("Team4", "Team1", 2, 0);
        Match match6 = new Match("Team4", "Team3", 2, 0);
        Match match7 = new Match("Team2", "Team6", 1, 1);

        Match[] matches = {
                match1, match2, match3, match4, match5, match6, match7
        };

        List<Match> matchesListSortByTeamName = Arrays.stream(matches).collect(Collectors.toList());

        LeagueTable leagueTable = new LeagueTable(matchesListSortByTeamName);

        List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntries();

        LeagueTableEntry team1 = leagueTableEntries.get(0);
        LeagueTableEntry team2 = leagueTableEntries.get(1);

        assertEquals("Team4", team1.getTeamName());
        assertEquals(6, team1.getPoints());
        // different getGoalDifference
        assertEquals(4, team1.getGoalDifference());

        assertEquals("Team2", team2.getTeamName());
        assertEquals(6, team2.getPoints());
        // different getGoalDifference
        assertEquals(1, team2.getGoalDifference());
    }

    @Test
    public void test_LeagueTableEntries_order_with_different_points() {

        Match match1 = new Match("Team2", "Team1", 2, 0);
        Match match2 = new Match("Team1", "Team2", 1, 0);
        Match match3 = new Match("Team3", "Team2", 1, 1);
        Match match4 = new Match("Team2", "Team3", 1, 1);

        Match[] matches = {
                match1, match2, match3, match4
        };

        List<Match> matchesListSortByTeamName = Arrays.stream(matches).collect(Collectors.toList());

        LeagueTable leagueTable = new LeagueTable(matchesListSortByTeamName);

        List<LeagueTableEntry> leagueTableEntries = leagueTable.getTableEntries();

        LeagueTableEntry team1 = leagueTableEntries.get(0);
        LeagueTableEntry team2 = leagueTableEntries.get(1);

        assertEquals("Team2", team1.getTeamName());
        // different getPoints
        assertEquals(5, team1.getPoints());


        assertEquals("Team1", team2.getTeamName());
        // different getPoints
        assertEquals(3, team2.getPoints());
    }
}
