package com.league.evaluation.exception;

/**
 * Created by Vladimir Usychenko on 4/4/16.
 */
public class TeamConfusionException extends RuntimeException {

    public TeamConfusionException() {
        super("Team can't play with itself");
    }
}
