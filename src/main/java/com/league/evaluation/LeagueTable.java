package com.league.evaluation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Vladimir Usychenko on 4/4/16.
 */
public class LeagueTable {

    private List<LeagueTableEntry> tableEntries;
    private Map<String, LeagueTableEntry> tableMap;

    /**
     * Create a league table from the supplied list of match results
     *
     * @param matches
     */
    public LeagueTable(final List<Match> matches) {

        // can't change method signature, so, let's check for null here
        if (matches != null) {

            this.tableMap = new HashMap<>();

            // ignore nulls in list
            matches.stream()
                    .filter(m -> m != null)
                    .forEach(match -> fillLeagueTablesFromMatch(match));

            this.tableEntries = this.tableMap.entrySet()
                    .stream()
                    .sorted((e1, e2) -> e1.getValue().compareTo(e2.getValue()))
                    .map(Map.Entry::getValue)
                    .collect(Collectors.toList());
        }
    }

    /**
     * Get the ordered list of league table entries for this league table.
     *
     * @return
     */
    public List<LeagueTableEntry> getTableEntries() {
        return this.tableEntries;
    }

    void fillLeagueTablesFromMatch(final Match match) {

        LeagueTableEntry homeTeam = putOrGetLeagueTableEntry(match.getHomeTeam());
        LeagueTableEntry awayTeam = putOrGetLeagueTableEntry(match.getAwayTeam());

        int homeScore = match.getHomeScore();
        int awayScore = match.getAwayScore();

        homeTeam.playMatch(homeScore, awayScore);
        awayTeam.playMatch(awayScore, homeScore);

    }

    LeagueTableEntry putOrGetLeagueTableEntry(String teamName) {

        LeagueTableEntry entry = this.tableMap.get(teamName);

        if (entry == null) {
            entry = new LeagueTableEntry(teamName);
            this.tableMap.put(teamName, entry);
        }

        return entry;
    }

}

