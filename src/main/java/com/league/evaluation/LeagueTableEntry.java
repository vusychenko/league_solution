package com.league.evaluation;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

/**
 * Created by Vladimir Usychenko on 4/4/16.
 */

// class needs equals and hashcode method to be stored properly inside HashMap and used inside all Java collections as well
@EqualsAndHashCode
public class LeagueTableEntry implements Comparable<LeagueTableEntry> {

    @Getter
    private String teamName;

    @Getter
    private int played;

    @Getter
    private int won;

    @Getter
    private int drawn;

    @Getter
    private int lost;

    @Getter
    private int goalsFor;

    @Getter
    private int goalsAgainst;

    @Getter
    private int goalDifference;

    @Getter
    private int points;

    public LeagueTableEntry(@NonNull String teamName) {
        this.teamName = teamName;
        // in fact we should call this(teamName, 0, 0, 0...), but all simple types will be initialized to zeros by default
        // so, in this case we can ignore
    }

    public LeagueTableEntry(@NonNull String teamName, int played, int won, int drawn, int lost,
                            int goalsFor, int goalsAgainst, int goalDifference, int points) {
        this.teamName = teamName;
        this.played = played;
        this.won = won;
        this.drawn = drawn;
        this.lost = lost;
        this.goalsFor = goalsFor;
        this.goalsAgainst = goalsAgainst;
        this.goalDifference = goalDifference;
        this.points = points;
    }

    public void playMatch(int goalsFor, int goalsAgainst) {
        this.goalsFor += goalsFor;
        this.goalsAgainst += goalsAgainst;
        this.played++;

        if (goalsFor == goalsAgainst) {
            this.drawn();
        } else {
            if (goalsFor > goalsAgainst) {
                this.won();
            } else {
                this.lost();
            }
            calculateGoalDifference();
        }
    }

    void won() {
        this.won++;
        this.points += 3;
    }

    void drawn() {
        this.drawn++;
        this.points += 1;
    }

    void lost() {
        this.lost++;
    }

    void calculateGoalDifference() {
        this.goalDifference = this.goalsFor - this.goalsAgainst;
    }

    @Override
    public int compareTo(LeagueTableEntry o) {
        if (this.points == o.getPoints()) {
            if (this.goalDifference == o.getGoalDifference()) {
                if (this.goalsFor == o.getGoalsFor()) {
                    // team can't play with itself
                    return this.getTeamName().compareTo(o.getTeamName());
                } else {
                    if (this.goalsFor > o.getGoalsFor()) {
                        return -1;
                    } else {
                        return 1;
                    }
                }
            } else {
                if (this.goalDifference > o.getGoalDifference()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        } else {
            if (this.points > o.getPoints()) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}

