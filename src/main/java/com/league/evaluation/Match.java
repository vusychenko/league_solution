package com.league.evaluation;

import com.league.evaluation.exception.TeamConfusionException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

/**
 * Created by Vladimir Usychenko on 4/4/16.
 */

@EqualsAndHashCode
public class Match {

    @Getter
    private String homeTeam;

    @Getter
    private String awayTeam;

    @Getter
    private int homeScore;

    @Getter
    private int awayScore;

    public Match(@NonNull final String homeTeam, @NonNull final String awayTeam, final int homeScore, final int awayScore) {
        if (homeTeam.equals(awayTeam)) {
            new TeamConfusionException();
        }
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeScore = homeScore;
        this.awayScore = awayScore;
    }

}

